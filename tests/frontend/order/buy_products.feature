Feature: Product purchases

  @tcId005
  Scenario: Add item to cart
    Given user already on home page
    When user selects a backpack
    Then item selected successfully

  @tcId006
  Scenario: Remove item from cart
    Given user already on home page
    When user clicks remove
    Then item will be removed

  @tcId007
  Scenario Outline: Checkout single item
    Given user already on home page
    When user selects a backpack
    And user open cart
    And user clicks checkout button
    And user enters the first name <first_name>
    And user enters the last name <last_name>
    And user enters postal code <postal_code>
    And user clicks continue button
    And user clicks finish button
    Then item successfully ordered

    Examples:
      | first_name | last_name  | postal_code |
      | Ahsan      | Khuluk     | 40911       |
      | Zaelani    | Zulkarnaen | 80451       |


  @tcId008
  Scenario Outline: Checkout multiple item
    Given user already on home page
    When user selects a backpack
    And user selects a red shirt
    And user open cart
    And user clicks checkout button
    And user enters the first name <first_name>
    And user enters the last name <last_name>
    And user enters postal code <postal_code>
    And user clicks continue button
    And user clicks finish button
    Then item successfully ordered

    Examples:
      | first_name | last_name | postal_code |
      | Ferdy      | Ciputra   | 46052       |
      | Maryo      | Osan      | 12456       |
      | ulfa       | Dwi       | 79954       |


  @tcId009
  Scenario Outline: Checkout random item
    Given user already on home page
    When user selects <qty> random items
    And user open cart
    And user clicks checkout button
    And user enters the first name <first_name>
    And user enters the last name <last_name>
    And user enters postal code <postal_code>
    And user clicks continue button
    And user clicks finish button
    Then item successfully ordered

    Examples:
      | first_name | last_name  | postal_code | qty |
      | Ahsan      | Khuluk     | 40911       | 2   |